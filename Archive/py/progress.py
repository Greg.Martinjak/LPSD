# Import results files for all seven test cases and compare true/false values for multiplicative/reductive
# and report out the progress made towards completion of all POIs evaluated
import os

a = os.listdir('Test Case - 1')

## REDUCTIVE
# Test Case 1: 47/47
# Test Case 2: 99/100
# Test Case 3: 99/112
# Test Case 4: 73/94 (Gable points not conisdered)
# Test Case 5: 119/119
# Test Case 6: 97/116
# Test Case 7: 56/66 (Gable points not conisdered)

## MULTIPLICATIVE
# Test Case 1: 45/47
# Test Case 2: 90/100
# Test Case 3: 0/112
# Test Case 4: 30/94 (Gable points not conisdered)
# Test Case 5: 75/119
# Test Case 6: /116
# Test Case 7: 7/66 (Gable points not conisdered)